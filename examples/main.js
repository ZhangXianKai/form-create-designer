import Vue from "vue";
import ELEMENT from "element-ui";
// import "element-ui/lib/theme-chalk/index.css";
import formCreate from "@form-create/element-ui";
import App from "./App";
import FcDesigner from "../src/index";

import VueRouter from "vue-router";
import routes from "./router/index.js";
Vue.use(VueRouter);
Vue.use(ELEMENT);
Vue.use(formCreate);
Vue.component("FcDesigner", FcDesigner);

Vue.config.productionTip = false;
const router = new VueRouter({
  mode: "history",
  base: "/work-module/",
  routes
});
new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
