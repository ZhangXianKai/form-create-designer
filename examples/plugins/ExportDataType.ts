/** 自定义表单数据格式 */
export interface InFormDataItem {
  /** 定制表单的id */
  id?: string;
  /** 定制表单的name */
  name?: string;
  /** 表单的类别 */
  type?: string;
  /** 定制表单的url */
  url?: string;
  /** 自定义表单json */
  customData?: InCustomData;
}
/** 自定义表单 */
export interface InCustomData {
  rule?: Array<InCustomDataRule>;
  options: {
    form: {
      hideRequiredAsterisk: boolean;
      inlineMessage: boolean;
      labelPosition: string;
      labelWidth: string;
      showMessage: boolean;
      size: string;
    };
  };
}
/** 自定义表单字段组 */
export interface InCustomDataRule {
  display: boolean;
  field: string;
  hidden: boolean;
  info: string;
  options: Array<{ value: string; label: string }>;
  title: string;
  type: string;
  validate: Array<any>;
  value: string;
}
