import App from "../App.vue";
import Index from "../Index.vue";
import Login from "../Login.vue";
const routes = [
  {
    path: "/login",
    name: "登录",
    component: Login
  },
  {
    path: "/",
    component: App,
    children: [
      {
        path: "/",
        name: "表单列表",
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: Index
      },
      {
        path: "/Modulelist",
        name: "表单列表",
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: Index
      }
    ]
  }
];

// const router = new VueRouter({
//   routes
// });

export default routes;
