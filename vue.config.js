module.exports = {
  pages: {
    app: {
      entry: "examples/main.js",
      template: "examples/index.html",
      filename: "index.html"
    }
  },
  publicPath: "/work-module/",
  productionSourceMap: false,
  configureWebpack: {
    resolve: {
      alias: {
        vue$: "vue/dist/vue.js",
        "@form-create/element-ui$":
          "@form-create/element-ui/dist/form-create.js"
      }
    }
  },
  devServer: {
    proxy: "http://192.168.3.252/"
    // {
    //   "/work-module/api": {
    //     target: "http://192.168.3.237:44341/", // 设置调用接口的域名和端口号
    //     // target: "http://192.168.3.213:8082", // 设置调用接口的域名和端口号
    //     // ws: true, // 是否启用websocket代理
    //     changeOrigin: true, // 是否跨域
    //     pathRewrite: {
    //       "/work-module/": "/"
    //     }
    //   },
    //   "/response_center": {
    //     target: "http://192.168.3.252/", // 设置调用接口的域名和端口号
    //     // target: "http://192.168.3.213:8082", // 设置调用接口的域名和端口号
    //     // ws: true, // 是否启用websocket代理
    //     changeOrigin: true, // 是否跨域
    //     pathRewrite: {}
    //   },
    //   "/publicData": {
    //     target: "http://192.168.3.252/", // 设置调用接口的域名和端口号
    //     // target: "http://192.168.3.213:8082", // 设置调用接口的域名和端口号
    //     // ws: true, // 是否启用websocket代理
    //     changeOrigin: true, // 是否跨域
    //     pathRewrite: {}
    //   },
    //   "/common_data": {
    //     target: "http://192.168.3.252/", // 设置调用接口的域名和端口号
    //     // target: "http://192.168.3.213:8082", // 设置调用接口的域名和端口号
    //     // ws: true, // 是否启用websocket代理
    //     changeOrigin: true, // 是否跨域
    //     pathRewrite: {}
    //   }
    // }
  }
};
